﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task_8
{
    class SelectedMembersOfSequence
    {
        static private List<int> Sequence = new List<int>();
        static private int k;

        static void Main(string[] args)
        {
            Input();
            Output();
        }

        static void Input()
        {
            //System.Console.Write("Please, insert sequence elements (integer, separated by spaces):\n");
            bool flag = false;

            string arguments = System.Console.ReadLine();
            string[] parts = arguments.Split(' ');
            int[] numeric_parts = new int[parts.Length];

            for (int i = 0; i < parts.Length; i++)
            {
                if (!int.TryParse(parts[i], out numeric_parts[i]))
                {
                    //Input();
                    return;
                }
            }

            k = numeric_parts[0];
            for (int i = 1; i < numeric_parts.Length && numeric_parts[i] != -1; i++)
            {
                Sequence.Add(numeric_parts[i]);
            }

        }


        static void Output()
        {

            for (int i = 0; i * k < Sequence.Count; i++)
            {
                System.Console.Write(Sequence[i * k]);
                System.Console.Write(' ');
            }
            System.Console.Write(-1);
        }
    }
}
