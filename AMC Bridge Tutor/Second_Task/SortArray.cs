﻿using System;
using System.Collections.Generic;
using System.Text;

    class SortArray
    {

        static private List<int> Sequence = new List<int>();
        static int size;

        static void Main(string[] args)
        {
            Input();
            Output();
        }

        static void Input()
        {
            //System.Console.Write("Please, insert sequence elements (integer, separated by spaces):\n");
            bool flag = false;

            string arguments = System.Console.ReadLine();
            string[] parts = arguments.Split(' ');
            int[] numeric_parts = new int[parts.Length];

            for (int i = 0; i < parts.Length; i++)
            {
                if (!int.TryParse(parts[i], out numeric_parts[i]))
                {
                    //Input();
                    return;
                }
            }

            //int[] numeric_parts = Array.ConvertAll<string, int>(parts, int.Parse);
            size = numeric_parts[0];
            for (int i = 1; i < size + 1 && i < numeric_parts.Length; i++)
            {
                Sequence.Add(numeric_parts[i]);
            }
        }


        static void Output()
        {
            int temp;
            for (int i = 0; i < Sequence.Count - 1; i++)
                for (int j = 0; j < Sequence.Count - 1; j++ )
                    if(Sequence[j] > Sequence[j + 1])
                    {
                        temp = Sequence[j + 1];
                        Sequence[j + 1] = Sequence[j];
                        Sequence[j] = temp;
                    }

             System.Console.Write(size);
            foreach(int element in Sequence)
            {
                System.Console.Write(' ');
                System.Console.Write(element);
            }
        }
    }
