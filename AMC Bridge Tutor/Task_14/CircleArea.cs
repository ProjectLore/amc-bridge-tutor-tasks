﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;

namespace Task_14
{
    class CircleArea
    {
        static private double length;

        static void Main(string[] args)
        {
            Input();
            Output();
        }

        static void Input()
        {

            string arguments = System.Console.ReadLine();
            string[] parts = arguments.Split(' ');
            double[] numeric_parts = new double[parts.Length];

            for (int i = 0; i < parts.Length; i++)
            {
                if (!double.TryParse(parts[i], NumberStyles.Number, CultureInfo.InvariantCulture, out numeric_parts[i]))
                {
                    return;
                }
            }

            length = numeric_parts[0];
        }


        static void Output()
        {
            System.Console.Write(String.Format("{0:0.00000000}", (length*length)/(4 * Math.PI)));
        }
    }
}
