﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;

namespace Task_17
{
    class Triangle
    {
        static private double a, b, c;

        static void Main(string[] args)
        {
            Input();
            Output();
        }

        static void Input()
        {
            //System.Console.Write("Please, insert sequence elements (integer, separated by spaces):\n");
            bool flag = false;

            string arguments = System.Console.ReadLine();
            string[] parts = arguments.Split(' ');
            double[] numeric_parts = new double[parts.Length];

            for (int i = 0; i < parts.Length; i++)
            {
                if (!double.TryParse(parts[i], NumberStyles.Number, CultureInfo.InvariantCulture, out numeric_parts[i]))
                {
                    //Input();
                    return;
                }
            }

            //int[] numeric_parts = Array.ConvertAll<string, int>(parts, int.Parse);
            a = numeric_parts[0];
            b = numeric_parts[1];
            c = numeric_parts[2];
        }


        static void Output()
        {
            double p = (a + b + c) / 2;
            System.Console.Write(String.Format("{0:0.00000000}", a + b + c));
            System.Console.Write(' ');
            System.Console.Write(String.Format("{0:0.00000000}", Math.Sqrt(p * (p - a) * (p - b) * (p - c))));
            System.Console.Write(' ');
            System.Console.Write(String.Format("{0:0.00000000}", Math.Sqrt(p * (p - a) * (p - b) * (p - c))/p));
        }
    }
}
