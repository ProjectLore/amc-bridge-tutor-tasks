﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task_11
{
    class SequenceWithoutRepeats
    {
        static private List<int> Sequence = new List<int>();

        static void Main(string[] args)
        {
            Input();
            Output();
        }

        static void Input()
        {
            //System.Console.Write("Please, insert sequence elements (integer, separated by spaces):\n");
            bool flag = false;

            string arguments = System.Console.ReadLine();
            string[] parts = arguments.Split(' ');
            int[] numeric_parts = new int[parts.Length];

            for (int i = 0; i < parts.Length; i++)
            {
                if (!int.TryParse(parts[i], out numeric_parts[i]))
                {
                    return;
                }
            }

            for (int i = 0; i < numeric_parts.Length && numeric_parts[i] != -1; i++)
            {
                Sequence.Add(numeric_parts[i]);
            }

        }


        static void Output()
        {
            for (int i = 0; i < Sequence.Count - 1; i++)
                for (int j = i + 1; j < Sequence.Count; j++)
                    if (Sequence[i] == Sequence[j])
                        Sequence.RemoveAt(j);

                    foreach (int element in Sequence)
                    {
                        System.Console.Write(element);
                        System.Console.Write(' ');
                    }
            System.Console.Write(-1);
        }
    }
}
