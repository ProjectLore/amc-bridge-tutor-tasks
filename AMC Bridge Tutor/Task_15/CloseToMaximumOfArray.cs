﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;

namespace Task_15
{
    class CloseToMaximumOfArray
    {
        static private List<double> CustomArray = new List<double>();
        static private double e;

        static void Main(string[] args)
        {
            Input();
            Output();
        }

        static void Input()
        {

            string arguments = System.Console.ReadLine();
            string[] parts = arguments.Split(' ');
            double[] numeric_parts = new double[parts.Length];

            for (int i = 0; i < parts.Length; i++)
            {
                if (!double.TryParse(parts[i], NumberStyles.Number, CultureInfo.InvariantCulture, out numeric_parts[i]))
                {
                    return;
                }
            }

            e = numeric_parts[0];
            for (int i = 0; i < numeric_parts.Length && numeric_parts[i] != -1; i++)
            {
                CustomArray.Add(numeric_parts[i]);
            }
        }


        static void Output()
        {
            double max = CustomArray[0];
            int encount = 0;
            foreach (double element in CustomArray)
                if (element >= max)
                    max = element;

            foreach (double element in CustomArray)
                if (max - element < e)
                    encount++;
            System.Console.Write(encount);
        }
    }
}
