﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task_10
{
    class SequenceOfDifferentPairs
    {

        static private List<int> Sequence1 = new List<int>();
        static private List<int> Sequence2 = new List<int>();
        static private List<int> Sequence3 = new List<int>();

        static void Main(string[] args)
        {
            Input();
            Output();
        }

        static void Input()
        {
            //System.Console.Write("Please, insert sequence elements (integer, separated by spaces):\n");
            bool flag = false;

            string arguments = System.Console.ReadLine();
            string[] parts = arguments.Split(' ');
            int[] numeric_parts = new int[parts.Length];

            for (int i = 0; i < parts.Length; i++)
            {
                if (!int.TryParse(parts[i], out numeric_parts[i]))
                {
                    //Input();
                    Environment.Exit(0);
                }
            }

            //int[] numeric_parts = Array.ConvertAll<string, int>(parts, int.Parse);
            for (int i = 0; i < numeric_parts.Length && numeric_parts[i] != -1; i++)
            {
                if (i == 0 || i % 2 == 0)
                    Sequence1.Add(numeric_parts[i]);
                else
                    Sequence2.Add(numeric_parts[i]);
            }

            for (int i = Sequence1.Count * 2 + 1; i < numeric_parts.Length && numeric_parts[i] != -1; i++)
            {
                Sequence3.Add(numeric_parts[i]);
            }

            if (Sequence1.Count != Sequence2.Count)
                Environment.Exit(0);
        }


        static void Output()
        {

            foreach (int searched in Sequence3)
            {
                bool flag = true;

                for (int i = 0; i < Sequence1.Count; i++)
                {
                    if (Sequence1[i] == searched)
                    {
                        System.Console.Write(Sequence2[i]);
                        System.Console.Write(' ');
                        flag = false;
                    }

                }

                if (flag)
                {
                    System.Console.Write(0);
                    System.Console.Write(' ');
                }
            }

            System.Console.Write(-1);
        }
    }
}
