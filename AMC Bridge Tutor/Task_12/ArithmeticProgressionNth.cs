﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task_12
{
    class ArithmeticProgressionNth
    {
        static private int a, d, n;

        static void Main(string[] args)
        {
            Input();
            Output();
        }

        static void Input()
        {
            //System.Console.Write("Please, insert sequence elements (integer, separated by spaces):\n");
            bool flag = false;

                string arguments = System.Console.ReadLine();
                string[] parts = arguments.Split(' ');
                int[] numeric_parts = new int[parts.Length];

                for (int i = 0; i < parts.Length; i++)
                {
                    if (!int.TryParse(parts[i], out numeric_parts[i]))
                    {
                        //Input();
                        return;
                    }
                }

                //int[] numeric_parts = Array.ConvertAll<string, int>(parts, int.Parse);
                a = numeric_parts[0];
                d = numeric_parts[1];
                n = numeric_parts[2];
        }


        static void Output()
        {
            System.Console.Write(a+(n-1)*d);
        }
    }
}
