﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task_6
{
    class RemainingElementsOfSequenceAfterSpecialOperations
    {

        static private List<int> Sequence1 = new List<int>();
        static private List<int> Sequence2 = new List<int>();

        static void Main(string[] args)
        {
            Input();
            Output();
        }

        static void Input()
        {
            //System.Console.Write("Please, insert sequence elements (integer, separated by spaces):\n");
            bool flag = false;

            string arguments = System.Console.ReadLine();
            string[] parts = arguments.Split(' ');
            int[] numeric_parts = new int[parts.Length];

            for (int i = 0; i < parts.Length; i++)
            {
                if (!int.TryParse(parts[i], out numeric_parts[i]))
                {
                    //Input();
                    return;
                }
            }

            //int[] numeric_parts = Array.ConvertAll<string, int>(parts, int.Parse);
            for (int i = 0; i < numeric_parts.Length && numeric_parts[i] != -1; i++)
            {
                Sequence1.Add(numeric_parts[i]);
            }

            for (int i = Sequence1.Count + 1; i < numeric_parts.Length && numeric_parts[i] != -1; i++)
            {
                Sequence2.Add(numeric_parts[i]);
            }
        }


        static void Output()
        {

            foreach(int number in Sequence2)
                if(number < 0)
                {
                    for(int i = 0; i > number; i --)
                        Sequence1.RemoveAt(Sequence1.Count - 1);
                }
                else if(number > 0)
                {
                    for(int i = 0; i < number; i++)
                        Sequence1.Add(number);
                }

            foreach(int element in Sequence1)
            {
                System.Console.Write(element);
                System.Console.Write(' ');
            }
            System.Console.Write(-1);
        }
    }
}
