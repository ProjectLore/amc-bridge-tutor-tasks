﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task_16
{
    class Cube
    {
        static private int edge;

        static void Main(string[] args)
        {
            Input();
            Output();
        }

        static void Input()
        {

            string arguments = System.Console.ReadLine();
            string[] parts = arguments.Split(' ');
            int[] numeric_parts = new int[parts.Length];

            for (int i = 0; i < parts.Length; i++)
            {
                if (!int.TryParse(parts[i], out numeric_parts[i]))
                {
                    return;
                }
            }

            edge = numeric_parts[0];
        }


        static void Output()
        {
            System.Console.Write(edge * edge * edge);
            System.Console.Write(' ');
            System.Console.Write((edge * edge)*6);
        }
    }
}
