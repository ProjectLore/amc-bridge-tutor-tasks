﻿using System;
using System.Collections.Generic;
using System.Text;

namespace First_Task
{
    class LongestIncreasingSubArray
    {

        static private List<int> Sequence = new List<int>();

        static void Main(string[] args)
        {
            Input();
            Output();
        }

        static void Input()
        {
            //System.Console.Write("Please, insert sequence elements (integer, separated by spaces):\n");
            bool flag = false;

                string arguments = System.Console.ReadLine();
                string[] parts = arguments.Split(' ');
                int[] numeric_parts = new int[parts.Length];

                for (int i = 0; i < parts.Length; i++)
                {
                    if (!int.TryParse(parts[i], out numeric_parts[i]))
                    {
                        //Input();
                        System.Console.Write(0);
                        return;
                    }
                }

                //int[] numeric_parts = Array.ConvertAll<string, int>(parts, int.Parse);
                int size = numeric_parts[0];
                for (int i = 1; i < size + 1 && i < numeric_parts.Length; i++)
                {
                    Sequence.Add(numeric_parts[i]);
                }
        }


        static void Output()
        {
            int longest = 1; int chain = 1;
            for(int i = 0; i < Sequence.Count - 1; i++)
                if (Sequence[i] < Sequence[i +1] )
                {
                    chain ++;
                }
            else
                {
                    if (chain > longest)
                        longest = chain;
                    chain = 1;
                }
            if (chain > longest)
                longest = chain;

            if (longest > 1)
                System.Console.Write(longest);
            else
                System.Console.Write(false);

        }
    }
}
