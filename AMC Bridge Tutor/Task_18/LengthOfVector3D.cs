﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;

namespace Task_18
{
    class LengthOfVector3D
    {

        static private List<double> x = new List<double>();
        static private List<double> y = new List<double>();
        static private List<double> z = new List<double>();
        static int count;

        static void Main(string[] args)
        {
            Input();
            Output();
        }

        static void Input()
        {
            bool flag = false;

            string arguments = System.Console.ReadLine();
            string[] parts = arguments.Split(' ');
            double[] numeric_parts = new double[parts.Length];


            if (!int.TryParse(parts[0], out count))
            {
                Environment.Exit(0);
            }

            for (int i = 1; i < parts.Length; i++)
            {
                if (!double.TryParse(parts[i], NumberStyles.Number, CultureInfo.InvariantCulture, out numeric_parts[i - 1]))
                {
                    Environment.Exit(0);
                }
            }

            for (int i = 0; i < count; i++)
                x.Add(numeric_parts[0 + (3 * i)]);
            for (int i = 0; i < count; i++)
                y.Add(numeric_parts[1 + (3 * i)]);
            for (int i = 0; i < count; i++)
                z.Add(numeric_parts[2 + (3 * i)]);
        }


        static void Output()
        {
            double max = 0;
            for(int i = 0; i < count; i++)
            {
                if(max < Math.Sqrt(x[i]*x[i] + y[i]*y[i] + z[i]*z[i]))
                    max = Math.Sqrt(x[i] * x[i] + y[i] * y[i] + z[i] * z[i]);
            }

            System.Console.Write(String.Format("{0:0.00000000}", max));
        }
    }
}
